﻿using System;
using System.IO;
using System.Linq;
using Main;
using Moq;
using Xunit;

namespace Tests {
    public class JoinerTests {
        [Fact]
        public void CanConstruct() {
            // ReSharper disable once ObjectCreationAsStatement
            new Joiner();
        }

        [Fact]
        public void Join_WithNoData_DoesNothing() {
            var provider = new Mock<IStreamProvider>();
            var joiner = new Joiner();

            joiner.Join(provider.Object, new MemoryStream(new byte[0]));

            provider.Verify(m => m.GetStream(), Times.Exactly(1));
        }

        [Fact]
        public void Join_WithOneInput_CopiesIt() {
            var provider = new Mock<IStreamProvider>();
            var joiner = new Joiner();

            var input = new byte[] { 0x01, 0x02 };
            provider.SetupSequence(m => m.GetStream())
                .Returns(new MemoryStream(input))
                .Returns(null);

            var output = new byte[2];
            joiner.Join(provider.Object, new MemoryStream(output));

            provider.Verify(m => m.GetStream(), Times.Exactly(2));
            Assert.Equal(input, output);
        }

        [Fact]
        public void Join_WithMultipleInputs_CopiesThemAll() {
            var provider = new Mock<IStreamProvider>();
            var joiner = new Joiner();

            var input1 = new byte[] { 0x01, 0x02 };
            var input2 = new byte[] { 0x01, 0x02 };
            provider.SetupSequence(m => m.GetStream())
                .Returns(new MemoryStream(input1))
                .Returns(new MemoryStream(input2))
                .Returns(null);

            var output = new byte[4];
            joiner.Join(provider.Object, new MemoryStream(output));

            provider.Verify(m => m.GetStream(), Times.Exactly(3));
            var expected = new byte[4];
            Array.Copy(input1, 0, expected, 0, 2);
            Array.Copy(input2, 0, expected, 2, 2);
            Assert.Equal(expected, output);
        }
    }
}
