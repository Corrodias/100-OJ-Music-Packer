﻿using System;
using System.IO;
using Main;
using Moq;
using Xunit;

namespace Tests {
    public class SplitterTests {
        [Fact]
        public void CanConstruct() {
            // ReSharper disable once ObjectCreationAsStatement
            new Splitter();
        }

        [Fact]
        public void Split_WithNoData_DoesNothing() {
            var provider = new Mock<IStreamProvider>();
            var splitter = new Splitter();

            splitter.Split(new MemoryStream(new byte[0]), provider.Object);

            provider.Verify(m => m.GetStream(), Times.Never);
        }

        [Fact]
        public void Split_WithNoOggData_ProducesOneFile() {
            var provider = new Mock<IStreamProvider>();
            var splitter = new Splitter();

            var output = new byte[10];
            provider.SetupSequence(m => m.GetStream()).Returns(new MemoryStream(output));
            var input = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            splitter.Split(new MemoryStream(input), provider.Object);

            provider.Verify(m => m.GetStream(), Times.Once);
            Assert.Equal(input, output);
        }

        [Fact]
        public void Split_WithOneOggFile_ProducesOneFile() {
            var provider = new Mock<IStreamProvider>();
            var splitter = new Splitter();

            var output = new byte[18];
            provider.SetupSequence(m => m.GetStream()).Returns(new MemoryStream(output));
            var input = new byte[] { 0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x0D, 0x0A };

            splitter.Split(new MemoryStream(input), provider.Object);

            provider.Verify(m => m.GetStream(), Times.Once);
            Assert.Equal(input, output);
        }

        [Fact]
        public void Split_WithFiveOggFiles_ProducesFiveFiles() {
            var provider = new Mock<IStreamProvider>();
            var splitter = new Splitter();

            var output1 = new byte[18];
            var output2 = new byte[18];
            var output3 = new byte[18];
            var output4 = new byte[18];
            var output5 = new byte[18];
            provider.SetupSequence(m => m.GetStream())
                .Returns(new MemoryStream(output1))
                .Returns(new MemoryStream(output2))
                .Returns(new MemoryStream(output3))
                .Returns(new MemoryStream(output4))
                .Returns(new MemoryStream(output5));
            var input = new byte[] {
                0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x01, 0x02,
                0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x03, 0x04,
                0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x05, 0x06,
                0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x07, 0x08,
                0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x09, 0x0A
            };

            splitter.Split(new MemoryStream(input), provider.Object);

            provider.Verify(m => m.GetStream(), Times.Exactly(5));
            Assert.Equal(new ArraySegment<byte>(input, 00, 18), output1);
            Assert.Equal(new ArraySegment<byte>(input, 18, 18), output2);
            Assert.Equal(new ArraySegment<byte>(input, 36, 18), output3);
            Assert.Equal(new ArraySegment<byte>(input, 54, 18), output4);
            Assert.Equal(new ArraySegment<byte>(input, 72, 18), output5);
        }
    }
}