﻿using System;
using System.IO;
using Main;
using Xunit;

namespace Tests {
    public class UtilTests {
        [Fact]
        public void FilenameToNumber_GivenNull_Throws() {
            Assert.ThrowsAny<ArgumentNullException>(() => Util.FilenameToNumber(null));
        }

        [Fact]
        public void FilenameToNumber_GivenNumericFilename_ReturnsTheNumber() {
            Assert.Equal(0, Util.FilenameToNumber(new FileInfo("/0.txt")));
            Assert.Equal(5, Util.FilenameToNumber(new FileInfo("/5.txt")));
            Assert.Equal(10, Util.FilenameToNumber(new FileInfo("/10.txt")));
        }

        [Fact]
        public void FilenameToNumber_GivenNonNumericFilename_ReturnsZero() {
            Assert.Equal(0, Util.FilenameToNumber(new FileInfo("/x.txt")));
            Assert.Equal(0, Util.FilenameToNumber(new FileInfo("/a.txt")));
            Assert.Equal(0, Util.FilenameToNumber(new FileInfo("/1.0.txt")));
            Assert.Equal(0, Util.FilenameToNumber(new FileInfo("/.txt")));
        }
    }
}
