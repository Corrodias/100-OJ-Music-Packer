﻿using System;
using System.IO;
using Main;
using Xunit;

namespace Tests {
    public class SequentialFileOutputStreamProviderTests {
        [Fact]
        public void CanConstruct() {
            // ReSharper disable once ObjectCreationAsStatement
            new SequentialFileOutputStreamProvider(Guid.NewGuid().ToString());
        }

        [Fact]
        public void Constructor_WithNullPath_Throws() {
            // ReSharper disable once ObjectCreationAsStatement
            Assert.Throws<ArgumentNullException>(() => new SequentialFileOutputStreamProvider(null));
        }

        [Fact]
        public void GetStream_WithValidPath_ProvidesAFileStream() {
            var tempPath = Path.GetTempFileName();
            new FileInfo(tempPath).Delete();
            var tempDir = new DirectoryInfo(tempPath);

            try {
                using (var stream = new SequentialFileOutputStreamProvider(tempPath).GetStream()) {
                    Assert.IsAssignableFrom<FileStream>(stream);
                    Assert.True(stream.CanWrite);
                    var fileName = ((FileStream) stream).Name;
                    Assert.True(fileName.StartsWith(tempPath));
                }
            } finally {
                if (tempDir.Parent != null) // just for safety. don't delete the drive's root dir.
                    tempDir.Delete(true);
            }
        }

        [Fact]
        public void GetStream_WithExistingFiles_WillNotOverwriteThem() {
            var tempPath = Path.GetTempFileName();
            new FileInfo(tempPath).Delete();
            var tempDir = new DirectoryInfo(tempPath);
            tempDir.Create();
            File.WriteAllBytes(Path.Combine(tempPath, "1.ogg"), new byte[] { });

            try {
                Assert.Throws<IOException>(() => {
                    using (new SequentialFileOutputStreamProvider(tempPath).GetStream()) {}
                });
            } finally {
                if (tempDir.Parent != null) // just for safety. don't delete the drive's root dir.
                    tempDir.Delete(true);
            }
        }
    }
}