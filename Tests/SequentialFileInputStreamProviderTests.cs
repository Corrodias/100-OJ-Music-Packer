﻿using System;
using System.IO;
using Main;
using Xunit;

namespace Tests {
    public class SequentialFileInputStreamProviderTests {
        [Fact]
        public void CanConstruct() {
            // ReSharper disable once ObjectCreationAsStatement
            new SequentialFileInputStreamProvider(".");
        }

        [Fact]
        public void Constructor_WithNullPath_Throws() {
            // ReSharper disable once ObjectCreationAsStatement
            Assert.Throws<ArgumentNullException>(() => new SequentialFileInputStreamProvider(null));
        }

        [Fact]
        public void Constructor_WithInvalidDirectory_Throws() {
            var tempPath = Path.GetTempFileName();
            new FileInfo(tempPath).Delete();

            Assert.ThrowsAny<DirectoryNotFoundException>(() => new SequentialFileInputStreamProvider(tempPath));
        }

        [Fact]
        public void GetStream_WithEmptyDirectory_ProvidesZeroStreams() {
            var tempPath = Path.GetTempFileName();
            new FileInfo(tempPath).Delete();
            var tempDir = new DirectoryInfo(tempPath);
            tempDir.Create();

            try {
                using (var stream = new SequentialFileInputStreamProvider(tempPath).GetStream()) {
                    Assert.Null(stream);
                }
            } finally {
                if (tempDir.Parent != null) // just for safety. don't delete the drive's root dir.
                    tempDir.Delete(true);
            }
        }

        [Fact]
        public void GetStream_WithConformantFiles_ReturnsOneStreamForEach() {
            var tempPath = Path.GetTempFileName();
            new FileInfo(tempPath).Delete();
            var tempDir = new DirectoryInfo(tempPath);
            tempDir.Create();
            File.WriteAllBytes(Path.Combine(tempPath, "1.ogg"), new byte[] { });
            File.WriteAllBytes(Path.Combine(tempPath, "2.ogg"), new byte[] { });
            File.WriteAllBytes(Path.Combine(tempPath, "3.ogg"), new byte[] { });

            try {
                var target = new SequentialFileInputStreamProvider(tempPath);
                for (int i = 0; i < 3; i++) {
                    using (var stream = target.GetStream()) {
                        Assert.IsAssignableFrom<FileStream>(stream);
                        Assert.True(stream.CanRead);
                        var fileName = ((FileStream) stream).Name;
                        Assert.True(fileName.StartsWith(tempPath));
                    }
                }
                using (var stream = target.GetStream()) {
                    Assert.Null(stream);
                }
            } finally {
                if (tempDir.Parent != null) // just for safety. don't delete the drive's root dir.
                    tempDir.Delete(true);
            }
        }

        [Fact]
        public void GetStream_WithConformantFiles_ReturnsStreamsInOrder() {
            var tempPath = Path.GetTempFileName();
            new FileInfo(tempPath).Delete();
            var tempDir = new DirectoryInfo(tempPath);
            tempDir.Create();
            File.WriteAllBytes(Path.Combine(tempPath, "3.ogg"), new byte[] { });
            File.WriteAllBytes(Path.Combine(tempPath, "10.ogg"), new byte[] { });
            File.WriteAllBytes(Path.Combine(tempPath, "2.ogg"), new byte[] { });

            try {
                var target = new SequentialFileInputStreamProvider(tempPath);
                using (var stream = target.GetStream()) {
                    var fileName = ((FileStream) stream).Name;
                    Assert.True(fileName.EndsWith("2.ogg"));
                }
                using (var stream = target.GetStream()) {
                    var fileName = ((FileStream)stream).Name;
                    Assert.True(fileName.EndsWith("3.ogg"));
                }
                using (var stream = target.GetStream()) {
                    var fileName = ((FileStream)stream).Name;
                    Assert.True(fileName.EndsWith("10.ogg"));
                }
            } finally {
                if (tempDir.Parent != null) // just for safety. don't delete the drive's root dir.
                    tempDir.Delete(true);
            }
        }
    }
}