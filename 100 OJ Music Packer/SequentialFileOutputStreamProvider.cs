﻿using System;
using System.IO;

namespace Main {
    public class SequentialFileOutputStreamProvider : IStreamProvider {
        private readonly string _path;
        private int _fileCount = 1;

        public SequentialFileOutputStreamProvider(string path) {
            if (path == null)
                throw new ArgumentNullException();
            _path = path;
        }

        public Stream GetStream() {
            var directoryInfo = new DirectoryInfo(_path);
            if (!directoryInfo.Exists)
                directoryInfo.Create();
            return File.Open(Path.Combine(_path, _fileCount++ + @".ogg"), FileMode.CreateNew, FileAccess.ReadWrite);
        }
    }
}
