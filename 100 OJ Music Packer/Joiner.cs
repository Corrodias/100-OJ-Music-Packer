﻿using System.Collections.Generic;
using System.IO;

namespace Main {
    public class Joiner {
        public void Join(IStreamProvider inputStreamProvider, Stream outputStream) {
            foreach (var stream in AllStreams(inputStreamProvider)) {
                stream.CopyTo(outputStream);
            }
        }

        private static IEnumerable<Stream> AllStreams(IStreamProvider provider) {
            Stream stream;
            while ((stream = provider.GetStream()) != null) {
                yield return stream;
            }
        }
    }
}