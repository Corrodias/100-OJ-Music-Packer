using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Main {
    public class SequentialFileInputStreamProvider : IStreamProvider {
        private readonly IEnumerator<FileInfo> _files;

        public SequentialFileInputStreamProvider(string path) {
            if (path == null)
                throw new ArgumentNullException();
            var directoryInfo = new DirectoryInfo(path);
            if (!directoryInfo.Exists)
                throw new DirectoryNotFoundException();
            _files = directoryInfo.GetFiles()
                .Where(it => Util.FilenameToNumber(it) != 0)
                .OrderBy(Util.FilenameToNumber)
                .GetEnumerator();
        }

        public Stream GetStream() {
            return !_files.MoveNext() ? null : _files.Current.OpenRead();
        }
    }
}