﻿using System;
using System.IO;

namespace Main {
    class Startup {
        static void Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine(@"This program requires one argument: either a music file to split into a directory or a directory to join into a file.");
                return;
            }

            if (new FileInfo(args[0]).Exists)
                using (var inputStream = File.Open(args[0], FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    new Splitter().Split(inputStream, new SequentialFileOutputStreamProvider(args[0] + "_unpacked"));
            else if (new DirectoryInfo(args[0]).Exists && args[0].EndsWith("_unpacked"))
                using (var outputStream = File.Open(args[0].Substring(0, args[0].Length - 9), FileMode.CreateNew, FileAccess.ReadWrite))
                    new Joiner().Join(new SequentialFileInputStreamProvider(args[0]), outputStream);
            else
                Console.WriteLine(@"Error: The input must be 1) a file or 2) a directory whose name ends with ""_unpacked");
        }
    }
}