using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Main {
    public class Splitter {
        private static readonly byte[] OggHeader = { 0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        public void Split(Stream inputStream, IStreamProvider outputStreamProvider) {
            var inputSize = inputStream.Length;
            if (inputSize == 0)
                return;

            var offsets = FindOffsets(inputStream, OggHeader);

            inputStream.Seek(0, SeekOrigin.Begin);
            foreach (var endOffset in offsets) {
                using (var output = outputStreamProvider.GetStream()) {
                    CopyBytes(endOffset - inputStream.Position, inputStream, output);
                }
            }
            using (var output = outputStreamProvider.GetStream()) {
                CopyBytes(inputSize - inputStream.Position, inputStream, output);
            }
        }

        private static List<long> FindOffsets(Stream stream, byte[] target) {
            List<long> offsets = new List<long>();
            var buffer = new byte[target.Length];

            // fill the buffer
            int readByte;
            for (int i = 0; i < target.Length; i++) {
                readByte = stream.ReadByte();
                if (readByte == -1)
                    return offsets;
                buffer[i] = (byte) readByte;
            }

            // find the offsets
            while ((readByte = stream.ReadByte()) != -1) {
                Array.Copy(buffer, 1, buffer, 0, buffer.Length - 1);
                buffer[buffer.Length - 1] = (byte)readByte;
                if (buffer.SequenceEqual(target))
                    offsets.Add(stream.Position - target.Length);
            }
            return offsets;
        }

        long CopyBytes(long bytesRequired, Stream inStream, Stream outStream) {
            long readSoFar = 0L;
            var buffer = new byte[64 * 1024];
            do {
                var toRead = Math.Min(bytesRequired - readSoFar, buffer.Length);
                var readNow = inStream.Read(buffer, 0, (int) toRead);
                if (readNow == 0)
                    break; // End of stream
                outStream.Write(buffer, 0, readNow);
                readSoFar += readNow;
            } while (readSoFar < bytesRequired);
            return readSoFar;
        }
    }
}