﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Main {
    public class Util {
        public static int FilenameToNumber(FileInfo it) {
            if (it == null)
                throw new ArgumentNullException();
            var dotIndex = it.Name.LastIndexOf('.');
            if (dotIndex < 0)
                return 0;
            int num;
            if (!int.TryParse(it.Name.Substring(0, dotIndex), out num))
                return 0;
            return num;
        }
    }
}