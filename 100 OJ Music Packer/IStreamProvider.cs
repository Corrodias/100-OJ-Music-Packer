﻿using System.IO;

namespace Main {
    public interface IStreamProvider {
        Stream GetStream();
    }
}
